RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
echo -e "${GREEN}=============================================${NC}"
echo -e "${GREEN}== Starting step 3    =======================${NC}"
echo -e "${GREEN}=============================================${NC}"

cd /root/arch-install

export NEWUSER=plowater
echo "freshen packman for real install"
pacman -Syy --noconfirm archlinux-keyring
pacman-key --populate archlinux 
reflector --country Australia --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
pacman -Syyyy

echo "Set Timezone"
ln -sf /usr/share/zoneinfo/Australia/Melbourne /etc/localtime


hwclock --systohc
timedatectl set-ntp true
#not the best as it removes all other
echo en_US.UTF-8 UTF-8 > /etc/locale.gen
echo en_AU.UTF-8  UTF-8 >> /etc/locale.gen
locale-gen
echo LANG=en_AU.UTF-8 >> /etc/locale.conf
echo "May need to set hostname"
echo Arch-VM >> /etc/hostname

echo "more packages"
pacman --noconfirm -S grub grub-btrfs efibootmgr base-devel linux-headers networkmanager network-manager-applet wpa_supplicant dialog os-prober mtools dosfstools reflector git openssh net-tools man 

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id = Arch
grub-mkconfig -o /boot/grub/grub.cfg
useradd -mG wheel $NEWUSER
echo "Enter password for $NEWUSER"
passwd $NEWUSER
echo "%wheel  ALL=(ALL)       ALL" >> /etc/sudoers

echo "Enable network"
systemctl enable NetworkManager
echo "Enable sshd"
systemctl enable sshd
## If you installed bluez
#systemctl enable bluetooth
## If you installed cups
#systemctl enable org.cups.cupsd

mkdir /home/${NEWUSER}/archlinux-setup
cp /root/archlinux-setup/4*.sh /home/${NEWUSER}/archlinux-setup
#fish prompt config
cp /root/archlinux-setup/*.fish /home/${NEWUSER}/archlinux-setup
chown ${NEWUSER} /home/${NEWUSER}/archlinux-setup
chown ${NEWUSER} /home/${NEWUSER}/archlinux-setup/*
chmod +x /home/${NEWUSER}/archlinux-setup/*.sh


echo "========================================="
echo " Install complete, exit then reboot and "
echo " run step 4 as user ${NEWUSER}"
echo "========================================="


