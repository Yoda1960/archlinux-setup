RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
EFI=`dmesg | grep -c EFI`
   echo -e "${GREEN}=============================================${NC}"
if [[ $EFI -gt 0 ]]
then 
echo -e "${GREEN}UEFI System${NC} is that correct Y/N"
else
echo -e "${GREEN}MBR  System${NC} is that correct Y/N"
fi
   echo -e "${GREEN}=============================================${NC}"
while true
do
read -r input
case $input in
     [yY][eE][sS]|[yY])
 break
 ;;
     [nN][oO]|[nN])
   echo -e "${GREEN}=============================================${NC}"
   echo -e "${RED}Cannot continue, don't know correct boot${NC}"
   echo -e "${GREEN}=============================================${NC}"
 
 exit 1
        ;;
     *)
 echo "Invalid input..."
 ;;
 esac
done

DISK=/dev/`lsblk | grep disk|cut -d" " -f1`
echo -e "Will use disk ${GREEN}${DISK}${NC} is that correct Y/N"
read -r input
case $input in
     [yY][eE][sS]|[yY])
 break
 ;;
     [nN][oO]|[nN])
   echo -e "${GREEN}=============================================${NC}"
   echo -e "${RED}Cannot continue, maybe script is wrong?${NC}"
   echo -e "${GREEN}=============================================${NC}"
 	exit 1
        ;;
     *)
exit 1 ;;
 esac


echo "Stop"

if [[ $EFI -gt 0 ]]
then 
  ./efi.sh $DISK
else
  ./mbr.sh $DISK
fi

echo -e "${GREEN}create swap${NC}"
mkswap ${DISK}2
swapon ${DISK}2
echo -e "${GREEN}create btrfs${NC}"
mkfs.btrfs ${DISK}3

mount ${DISK}3 /mnt
btrfs su cr /mnt/@
umount /mnt
mount -o noatime,commit=120,compress=zstd,space_cache,subvol=@ ${DISK}3 /mnt
mkdir /mnt/boot
mkdir /mnt/.snapshots
mount /dev/sda1 /mnt/boot
echo -e "${GREEN}Disk complete${NC}"
./2-Base.sh

