GREEN='\033[0;32m'
NC='\033[0m'
echo -e "${GREEN}Set fish prompt${NC}"
mkdir -p ~/.config/fish/functions/
cp -p ~/archlinux-setup/*.fish ~/.config/fish/functions/
echo -e "${GREEN}Install last packages${NC}"
sudo pacman --noconfirm -S lightdm xorg-server fish vlc lxqt lightdm-gtk-greeter
echo -e "${GREEN}enable services${NC}"
sudo systemctl enable lightdm
sudo systemctl enable sshd
echo -e "${GREEN}install yay${NC}"
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
echo -e "${GREEN}Installing Browser${NC}"
yay -S brave-bin
while true
echo -e "${GREEN}Install VS Code${NC}"
do
read -r -p "[Y/n] " input
 case $input in
    [yY][eE][sS]|[yY])
	yay -S visual-studio-code-bin --answerclean Y --answerdiff N
	break
 ;;
    [nN][oO]|[nN])
	break
 ;;
     *)
 echo "Invalid input..."
 ;;
 esac
done

echo -e "${GREEN}=============================================${NC}"
echo -e "${GREEN}Install is complete, press ctrl-C${NC}"
echo -e "${GREEN}within 10 seconds to stop X11 start${NC}"
echo -e "${GREEN}=============================================${NC}"
sleep 10
sudo systemctl restart lightdm
