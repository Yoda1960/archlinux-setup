GREEN='\033[0;32m'
NC='\033[0m'
echo -e "${GREEN}###    freshen packman${NC}"
pacman -Syy --noconfirm archlinux-keyring
pacman-key --populate archlinux 
reflector --country Australia --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
pacman -Syyyy

echo -e "${GREEN}###    base system${NC}"
#intel
pacstrap /mnt base linux linux-firmware vim intel-ucode btrfs-progs git 
#amd
#pacstrap /mnt base linux linux-firmware vim amd-ucode btrfs-progs git 
#vm
#pacstrap /mnt base linux linux-firmware vim btrfs-progs git
echo -e "${GREEN}###    create fstab${NC}"
genfstab -U /mnt >> /mnt/etc/fstab
cp -r /root/archlinux-setup /mnt/root/archlinux-setup
chmod +x /mnt/root/archlinux-setup/*.sh
echo -e "${GREEN}###    switch root directory, step 3 in arch-chroot /root/archlinux-setup/${NC}"
arch-chroot /mnt /root/archlinux-setup/3-system.sh
