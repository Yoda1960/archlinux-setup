curl -sSL https://install.python-poetry.org | python3 -

Add `export PATH="/home/plowater/.local/bin:$PATH"` to your shell configuration file.

set -U fish_user_paths /home/plowater/.local/bin $fish_user_paths
set -U fish_user_paths eval /home/plowater/.pyenv/shims $fish_user_paths


Command Completions
# Bash
poetry completions bash > /etc/bash_completion.d/poetry

# Fish
poetry completions fish > ~/.config/fish/completions/poetry.fish
